from common import Timer


def activity_selector_recursive(activities, i=None, n=None):
    if i is None:
        i = -1
    if n is None:
        n = len(activities)

    m = i + 1
    while m < n-1 and i > -1 and activities[m][0] < activities[i][1]:
        m += 1
    if m < n:
        return [activities[m]] + activity_selector_recursive(activities, m, n)
    return []


def activity_selector(activities):
    n = len(activities)
    result = [activities[0]]
    i = 0

    for m in range(1, n):
        if activities[m][0] >= activities[i][1]:
            result.append(activities[m])
            i = m
    return result


if __name__ == '__main__':  # pragma: no cover
    with Timer('Activity Selector (recursive)'):
        selection = activity_selector_recursive(
            activities=[(1, 4), (3, 5), (0, 6), (5, 7), (3, 8), (5, 9), (6, 10), (8, 11), (8, 12), (2, 13), (12, 14)])
        print(selection)

    with Timer('Activity Selector'):
        selection = activity_selector(
            activities=[(1, 4), (3, 5), (0, 6), (5, 7), (3, 8), (5, 9), (6, 10), (8, 11), (8, 12), (2, 13), (12, 14)])
        print(selection)
