from common import Timer, random_numbers


class UnderflowError(Exception):
    pass


class Node:
    def __init__(self, data=None):
        self.data = data
        self.next = None


class Stack:
    def __init__(self):
        self.top = None

    @property
    def is_empty(self):
        return self.top is None

    def push(self, data):
        node = Node(data)
        node.next = self.top
        self.top = node

    def pop(self):
        if self.is_empty:
            raise UnderflowError()
        node = self.top
        self.top = self.top.next
        return node.data


class Queue:
    def __init__(self):
        self.tail = None
        self.head = None

    @property
    def is_empty(self):
        return self.head is None

    def enqueue(self, data):
        node = Node(data)
        if self.tail is not None:
            self.tail.next = node
        self.tail = node
        if self.head is None:
            self.head = self.tail

    def dequeue(self):
        if self.is_empty:
            raise UnderflowError()
        node = self.head
        self.head = self.head.next
        return node.data


class LinkedList:
    def __init__(self):
        self.head = None

    @property
    def is_empty(self):
        return self.head is None

    def search(self, data):
        node = self.head
        while node is not None and node.data != data:
            node = node.next
        return node

    def insert_front(self, node):
        if not isinstance(node, Node):
            node = Node(node)
        node.next = self.head
        self.head = node

    def insert_after(self, node, data):
        if not isinstance(node, Node):
            node = Node(node)
        other = self.search(data)
        node.next = other.next
        other.next = node

    def delete(self, node):
        if self.head is node:
            self.head = self.head.next
        else:
            pointer = self.head
            while pointer is not None and pointer.next is not node:
                pointer = pointer.next
            if pointer is not None:
                pointer.next = node.next

    def traverse(self):
        node = self.head
        while node is not None:
            yield node.data
            node = node.next


if __name__ == '__main__':  # pragma: no cover
    with Timer('Generating Random list'):
        numbers = random_numbers()

    with Timer('Stack test'):
        s = Stack()
        for x in numbers:
            s.push(x)
        while not s.is_empty:
            s.pop()

    with Timer('Queue test'):
        s = Queue()
        for x in numbers:
            s.enqueue(x)
        while not s.is_empty:
            s.dequeue()

    with Timer('Linked List test'):
        ll = LinkedList()
        for x in numbers:
            ll.insert_front(x)
        ll.traverse()
