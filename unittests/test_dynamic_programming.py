from unittest import TestCase

from dynamic_programming import assembly_line_scheduling, matrix_chain_order, get_optimal_matrix_chain, \
    longest_common_subsequence, get_longest_common_subsequence, knapsack, get_knapsack_contents


class TestDynamicProgramming(TestCase):
    def test_assembly_line_scheduling(self):
        fastest, line, route = assembly_line_scheduling(
            assembly=[[7, 9, 3, 4, 8, 4], [8, 5, 6, 4, 5, 7]],
            transfer=[[2, 3, 1, 3, 4], [2, 1, 2, 2, 1]],
            entry=[2, 4], exit=[3, 2], nodes=6
        )
        self.assertEqual(fastest, 38)
        self.assertEqual(line, 0)
        self.assertListEqual(route, [0, 1, 0, 1, 1, 0])

    def test_assembly_line_scheduling_reversed(self):
        fastest, line, route = assembly_line_scheduling(
            assembly=[[8, 5, 6, 4, 5, 7], [7, 9, 3, 4, 8, 4]],
            transfer=[[2, 1, 2, 2, 1], [2, 3, 1, 3, 4]],
            entry=[4, 2], exit=[2, 3], nodes=6
        )
        self.assertEqual(fastest, 38)
        self.assertEqual(line, 1)
        self.assertListEqual(route, [1, 0, 1, 0, 0, 1])

    def test_matrix_chain_order(self):
        m, s = matrix_chain_order([30, 35, 15, 5, 10, 20, 25])
        self.assertListEqual(m, [[0, 15750, 7875, 9375, 11875, 15125],
                                 [0, 0, 2625, 4375, 7125, 10500],
                                 [0, 0, 0, 750, 2500, 5375],
                                 [0, 0, 0, 0, 1000, 3500],
                                 [0, 0, 0, 0, 0, 5000],
                                 [0, 0, 0, 0, 0, 0]])
        self.assertListEqual(s, [[0, 0, 0, 2, 2, 2],
                                 [0, 0, 1, 2, 2, 2],
                                 [0, 0, 0, 2, 2, 2],
                                 [0, 0, 0, 0, 3, 4],
                                 [0, 0, 0, 0, 0, 4],
                                 [0, 0, 0, 0, 0, 0]])
        optimal = get_optimal_matrix_chain(s)
        self.assertEqual(optimal, '((A0(A1A2))((A3A4)A5))')

    def test_longest_common_subsequence(self):
        s1 = 'abcbdab'
        s2 = 'bdcaba'
        length_table = longest_common_subsequence(s1, s2)
        self.assertListEqual(length_table, [[0, 0, 0, 0, 0, 0, 0],
                                            [0, 0, 0, 0, 1, 1, 1],
                                            [0, 1, 1, 1, 1, 2, 2],
                                            [0, 1, 1, 2, 2, 2, 2],
                                            [0, 1, 1, 2, 2, 3, 3],
                                            [0, 1, 2, 2, 2, 3, 3],
                                            [0, 1, 2, 2, 3, 3, 4],
                                            [0, 1, 2, 2, 3, 4, 4]])
        sequence = get_longest_common_subsequence(length_table, s1, len(s1), len(s2))
        self.assertEqual(sequence, 'bcba')

    def test_knapsack(self):
        items = [(10, 60), (20, 100), (30, 120)]
        total = 50
        value_table = knapsack(items=items, total=total)
        result = get_knapsack_contents(items, value_table)
        self.assertSetEqual(set(result), {(20, 100), (30, 120)})
