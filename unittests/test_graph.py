from unittest import TestCase

from graphs import Graph, Node


class TestGraph(TestCase):
    def test_constructor_directed(self):
        g = Graph(is_directed=True)
        g.add_nodes('a', 'b', 'c', 'd')
        g.add_edges((0, 1), (1, 2), (2, 3))
        self.assertEqual(len(g.nodes), 4)
        self.assertEqual(len(g.edges), 3)
        for index, node in enumerate(g.nodes):
            self.assertIsInstance(node, Node)
            self.assertEqual(len(node.edges), 1 if index < 3 else 0)
        self.assertListEqual(g.nodes[0].edges, [(1, None)])
        self.assertListEqual(g.nodes[1].edges, [(2, None)])
        self.assertListEqual(g.nodes[2].edges, [(3, None)])
        self.assertListEqual(g.nodes[3].edges, [])

    def test_constructor_not_directed(self):
        g = Graph(is_directed=False)
        g.add_nodes('a', 'b', 'c', 'd')
        g.add_edges((0, 1), (1, 2), (2, 3))
        self.assertEqual(len(g.nodes), 4)
        self.assertEqual(len(g.edges), 3)
        for index, node in enumerate(g.nodes):
            self.assertIsInstance(node, Node)
            self.assertEqual(len(node.edges), 2 if 0 < index < 3 else 1)
        self.assertListEqual(g.nodes[0].edges, [(1, None)])
        self.assertListEqual(g.nodes[1].edges, [(0, None), (2, None)])
        self.assertListEqual(g.nodes[2].edges, [(1, None), (3, None)])
        self.assertListEqual(g.nodes[3].edges, [(2, None)])

    def test_representation(self):
        g = Graph(is_directed=True)
        g.add_nodes('a', 'b', 'c', 'd')
        g.add_edges((0, 1), (1, 2), (2, 3))
        rep = repr(g).split('\n')
        self.assertEqual(rep[0], '0: a -> 1')
        self.assertEqual(rep[1], '1: b -> 2')
        self.assertEqual(rep[2], '2: c -> 3')
        self.assertEqual(rep[3], '3: d -> -')

    def test_breadth_first_search(self):
        g = Graph()
        g.add_nodes('r', 's', 't', 'u', 'v', 'w', 'x', 'y')
        g.add_edges((0, 1), (4, 0), (1, 5), (2, 5), (2, 3), (2, 6), (3, 6), (3, 7), (5, 6), (6, 7))
        dist, pred = g.breadth_first_search('s')
        self.assertListEqual(dist, [1, 0, 2, 3, 2, 1, 2, 3])
        self.assertListEqual(pred, [1, None, 5, 2, 0, 1, 5, 6])

    def test_depth_first_search(self):
        g = Graph(is_directed=True)
        g.add_nodes('u', 'v', 'w', 'x', 'y', 'z')
        g.add_edges((0, 1), (0, 3), (1, 4), (2, 4), (2, 5), (3, 1), (4, 3), (5, 5))
        disc, finish, pred = g.depth_first_search()
        self.assertListEqual(disc, [1, 2, 9, 4, 3, 10])
        self.assertListEqual(finish, [8, 7, 12, 5, 6, 11])
        self.assertListEqual(pred, [None, 0, None, 4, 1, 2])

    def test_topological_sort(self):
        g = Graph(is_directed=True)
        g.add_nodes('shirt', 'tie', 'jacket', 'belt', 'watch', 'undershorts', 'pants', 'shoes', 'socks')
        g.add_edges((0, 1), (1, 2), (0, 3), (3, 2), (5, 6), (5, 7), (6, 7), (6, 3), (8, 7))
        result = g.topological_sort()
        self.assertEqual(len(result), len(g.nodes))
        for r in result:
            self.assertIsInstance(r, Node)
        self.assertListEqual([r.data for r in result],
                             ['socks', 'undershorts', 'pants', 'shoes', 'watch', 'shirt', 'belt', 'tie', 'jacket'])

    def test_transpose_graph_directed(self):
        g = Graph(is_directed=True)
        g.add_nodes('u', 'v', 'w', 'x', 'y', 'z')
        g.add_edges((0, 1), (0, 3), (1, 4), (2, 4), (2, 5), (3, 1), (4, 3), (5, 5))
        gt = g.get_transpose()
        self.assertIsInstance(gt, Graph)
        self.assertListEqual([n.data for n in g.nodes], [n.data for n in gt.nodes])
        self.assertListEqual([(i, j, w) for i, j, w in g.edges], [(j, i, w) for i, j, w in gt.edges])

    def test_transpose_graph_non_directed(self):
        g = Graph(is_directed=False)
        g.add_nodes('u', 'v', 'w', 'x', 'y', 'z')
        g.add_edges((0, 1), (0, 3), (1, 4), (2, 4), (2, 5), (3, 1), (4, 3), (5, 5))
        gt = g.get_transpose()
        self.assertIsInstance(gt, Graph)
        self.assertListEqual([n.data for n in g.nodes], [n.data for n in gt.nodes])
        self.assertListEqual([(i, j, w) for i, j, w in g.edges], [(j, i, w) for i, j, w in gt.edges])

    def test_strongly_connected_components(self):
        g = Graph(is_directed=True)
        g.add_nodes('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h')
        g.add_edges((0, 1), (1, 2), (1, 4), (1, 5), (2, 3), (2, 6), (3, 2),
                    (3, 7), (4, 0), (4, 5), (5, 6), (6, 5), (6, 7), (7, 7))
        components = g.strongly_connected_components()
        self.assertEqual(len(components), 4)
        self.assertSetEqual(set(components[0]), {'a', 'b', 'e'})
        self.assertSetEqual(set(components[1]), {'c', 'd'})
        self.assertSetEqual(set(components[2]), {'f', 'g'})
        self.assertSetEqual(set(components[3]), {'h'})

    def test_minimum_spanning_trees_kruskal(self):
        g = Graph(is_weighted=True)
        g.add_nodes('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i')
        g.add_edges((0, 1, 4), (0, 7, 8), (1, 2, 8), (1, 7, 11), (2, 3, 7), (2, 5, 4), (2, 8, 2),
                    (3, 4, 9), (3, 5, 14), (4, 5, 10), (5, 6, 2), (6, 7, 1), (6, 8, 6), (7, 8, 7))
        result = g.minimum_spanning_trees_kruskal()
        self.assertSetEqual(set(result), {(6, 7), (2, 8), (5, 6), (0, 1), (2, 5), (2, 3), (0, 7), (3, 4)})

    def test_minimum_spanning_trees_prim(self):
        g = Graph(is_weighted=True)
        g.add_nodes('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i')
        g.add_edges((0, 1, 4), (0, 7, 8), (1, 2, 8), (1, 7, 11), (2, 3, 7), (2, 5, 4), (2, 8, 2),
                    (3, 4, 9), (3, 5, 14), (4, 5, 10), (5, 6, 2), (6, 7, 1), (6, 8, 6), (7, 8, 7))
        tree_edges, predecessor = g.minimum_spanning_trees_prim('a')
        self.assertSetEqual(set(tree_edges), {(0, 1), (1, 2), (2, 8), (2, 5), (5, 6), (6, 7), (2, 3), (3, 4)})
        self.assertListEqual(predecessor, [None, 0, 1, 2, 3, 2, 5, 6, 2])

    def test_shortest_path_bellman_ford(self):
        g = Graph(is_directed=True, is_weighted=True)
        g.add_nodes('s', 't', 'x', 'y', 'z')
        g.add_edges((0, 1, 6), (0, 3, 7), (1, 2, 5), (1, 3, 8), (1, 4, -4),
                    (2, 1, -2), (3, 2, -3), (3, 4, 9), (4, 0, 2), (4, 2, 7))
        distance, predecessor = g.shortest_path_bellman_ford('s')
        self.assertListEqual(distance, [0, 2, 4, 7, -2])
        self.assertListEqual(predecessor, [None, 2, 3, 0, 1])

    def test_shortest_path_dijkstra(self):
        g = Graph(is_directed=True, is_weighted=True)
        g.add_nodes('s', 't', 'x', 'y', 'z')
        g.add_edges((0, 1, 10), (0, 3, 5), (1, 2, 1), (1, 3, 2), (2, 4, 4),
                    (3, 1, 3), (3, 2, 9), (3, 4, 2), (4, 0, 7), (4, 2, 6))
        distance, predecessor = g.shortest_path_dijkstra('s')
        self.assertListEqual(distance, [0, 8, 9, 5, 7])
        self.assertListEqual(predecessor, [None, 3, 1, 0, 3])

