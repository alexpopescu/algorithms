import operator
from unittest import TestCase

from common import random_numbers
from sort import bubblesort, insertionsort, mergesort, heapsort, quicksort, countingsort, radixsort, minmax


class TestSort(TestCase):
    def setUp(self):
        self.numbers = random_numbers(length=100)

    def check_sorted(self, a, comparison):
        # lengths should be the same
        self.assertEqual(len(a), len(self.numbers))
        # a should be sorted according to the given comparison
        self.assertTrue(all(comparison(a[i], a[i+1]) for i in range(len(a)-1)))
        # the lists should contain the same elements
        self.assertSetEqual(set(a), set(self.numbers))


class TestBubbleSort(TestSort):
    def test_bubblesort_asc(self):
        self.check_sorted(bubblesort(self.numbers), comparison=operator.le)

    def test_bubblesort_desc(self):
        self.check_sorted(bubblesort(self.numbers, comparison=operator.gt), comparison=operator.ge)


class TestInsertionSort(TestSort):
    def test_insertionsort_asc(self):
        self.check_sorted(insertionsort(self.numbers), comparison=operator.le)

    def test_insertionsort_desc(self):
        self.check_sorted(insertionsort(self.numbers, comparison=operator.gt), comparison=operator.ge)


class TestMergeSort(TestSort):
    def test_mergesort_asc(self):
        self.check_sorted(mergesort(self.numbers), comparison=operator.le)

    def test_mergesort_desc(self):
        self.check_sorted(mergesort(self.numbers, comparison=operator.gt), comparison=operator.ge)


class TestHeapSort(TestSort):
    def test_heapsort_asc(self):
        self.check_sorted(heapsort(self.numbers), comparison=operator.le)

    def test_heapsort_desc(self):
        self.check_sorted(heapsort(self.numbers, comparison=operator.gt), comparison=operator.ge)


class TestQuickSort(TestSort):
    def test_quicksort_asc(self):
        self.check_sorted(quicksort(self.numbers), comparison=operator.le)

    def test_quicksort_desc(self):
        self.check_sorted(quicksort(self.numbers, comparison=operator.gt), comparison=operator.ge)


class TestCountingSort(TestSort):
    def test_countingsort_asc(self):
        self.check_sorted(countingsort(self.numbers), comparison=operator.le)


class TestRadixSort(TestSort):
    def test_radixsort_asc(self):
        self.check_sorted(radixsort(self.numbers), comparison=operator.le)


class TestMinMax(TestSort):
    def test_minmax(self):
        min1 = min(self.numbers)
        max1 = max(self.numbers)
        min2, max2 = minmax(self.numbers)
        self.assertEqual(min1, min2)
        self.assertEqual(max1, max2)

    def test_minmax_empty(self):
        min_, max_ = minmax([])
        self.assertIsNone(min_)
        self.assertIsNone(max_)

    def test_minmax_odd(self):
        numbers = list(self.numbers)[1:]
        min1 = min(numbers)
        max1 = max(numbers)
        min2, max2 = minmax(numbers)
        self.assertEqual(min1, min2)
        self.assertEqual(max1, max2)
