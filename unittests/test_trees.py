from random import shuffle
from unittest import TestCase

from trees import BinarySearchTree, TreeNode


class TestBinarySearchTree(TestCase):
    def setUp(self):
        self.numbers = list(range(10))
        self.shuffled_numbers = list(self.numbers)
        shuffle(self.shuffled_numbers)

    def test_bst_sort(self):
        t = BinarySearchTree()
        for x in self.shuffled_numbers:
            t.insert(x)
        self.assertListEqual(list(t.walk()), self.numbers)

    def test_bst_walk(self):
        t = BinarySearchTree()
        t.root = TreeNode(2)
        t.root.left = TreeNode(1)
        t.root.right = TreeNode(3)
        self.assertListEqual(list(t.walk()), [1, 2, 3])
        self.assertListEqual(list(t.walk(order='pre')), [2, 1, 3])
        self.assertListEqual(list(t.walk(order='post')), [1, 3, 2])

    def test_bst_search(self):
        t = BinarySearchTree()
        t.root = TreeNode(2)
        t.root.left = TreeNode(1)
        t.root.right = TreeNode(3)

        n1 = t.search(2)
        self.assertIsNotNone(n1)
        self.assertIsInstance(n1, TreeNode)

        n2 = t.search(4)
        self.assertIsNone(n2)

    def test_bst_search_recursive(self):
        t = BinarySearchTree()
        t.root = TreeNode(2)
        t.root.left = TreeNode(1)
        t.root.right = TreeNode(3)

        n1 = t.search_recursive(2)
        self.assertIsNotNone(n1)
        self.assertIsInstance(n1, TreeNode)

        n2 = t.search_recursive(4)
        self.assertIsNone(n2)

    def test_bst_minmax(self):
        t = BinarySearchTree()
        for x in self.shuffled_numbers:
            t.insert(x)

        min_ = t.min()
        self.assertIsInstance(min_, TreeNode)
        self.assertEqual(min_.data, 0)

        max_ = t.max()
        self.assertIsInstance(max_, TreeNode)
        self.assertEqual(max_.data, 9)

    def test_bst_empty(self):
        t = BinarySearchTree()
        self.assertIsNone(t.min())
        self.assertIsNone(t.max())
        self.assertIsNone(t.successor(None))
        self.assertIsNone(t.predecessor(None))

    def test_bst_successor(self):
        t = BinarySearchTree()
        for x in self.shuffled_numbers:
            t.insert(x)

        for x in range(9):
            succ = t.successor(t.search(x))
            self.assertIsInstance(succ, TreeNode)
            self.assertEqual(succ.data, x+1)
        self.assertIsNone(t.successor(t.max()))

    def test_bst_predecessor(self):
        t = BinarySearchTree()
        for x in self.shuffled_numbers:
            t.insert(x)

        for x in range(1, 10):
            pred = t.predecessor(t.search(x))
            self.assertIsInstance(pred, TreeNode)
            self.assertEqual(pred.data, x-1)
        self.assertIsNone(t.predecessor(t.min()))

    def test_bst_delete(self):
        t = BinarySearchTree()
        for x in self.shuffled_numbers:
            t.insert(x)

        for x in self.numbers:
            t.delete(t.search(x))
            self.numbers.remove(x)
            self.assertListEqual(list(t.walk()), self.numbers)

    # def test_bst_delete_root(self):
    #     t = BinarySearchTree()
    #     t.root = TreeNode(2)
    #     t.root.left = TreeNode(1)
    #     t.root.right = TreeNode(3)
    #     t.delete(t.root)
    #     self.assertListEqual(list(t.walk()), [1, 3])
