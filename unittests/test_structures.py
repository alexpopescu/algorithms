from unittest import TestCase

from common import random_numbers
from structures import Stack, Queue, LinkedList, UnderflowError


class TestStack(TestCase):
    def test_stack_empty(self):
        s = Stack()
        self.assertTrue(s.is_empty)

    def test_stack_underflow(self):
        s = Stack()
        with self.assertRaises(UnderflowError):
            s.pop()

    def test_stack_push(self):
        s = Stack()
        s.push(1)
        self.assertFalse(s.is_empty)

    def test_stack_pop(self):
        s = Stack()
        s.push(1)
        s.push(2)
        x = s.pop()
        self.assertEqual(x, 2)
        x = s.pop()
        self.assertEqual(x, 1)
        self.assertTrue(s.is_empty)

    def test_stack_multi(self):
        numbers = random_numbers(length=100)
        s = Stack()
        for x in numbers:
            s.push(x)
        out = []
        while not s.is_empty:
            out.append(s.pop())
        self.assertListEqual(out, list(reversed(numbers)))


class TestQueue(TestCase):
    def test_queue_empty(self):
        q = Queue()
        self.assertTrue(q.is_empty)

    def test_queue_underflow(self):
        q = Queue()
        with self.assertRaises(UnderflowError):
            q.dequeue()

    def test_queue_push(self):
        q = Queue()
        q.enqueue(1)
        self.assertFalse(q.is_empty)

    def test_queue_pop(self):
        q = Queue()
        q.enqueue(1)
        q.enqueue(2)
        x = q.dequeue()
        self.assertEqual(x, 1)
        x = q.dequeue()
        self.assertEqual(x, 2)
        self.assertTrue(q.is_empty)

    def test_queue_multi(self):
        numbers = random_numbers(length=100)
        q = Queue()
        for x in numbers:
            q.enqueue(x)
        out = []
        while not q.is_empty:
            out.append(q.dequeue())
        self.assertListEqual(out, numbers)


class TestLinkedList(TestCase):
    def test_list_empty(self):
        ll = LinkedList()
        self.assertTrue(ll.is_empty)

    def test_list_insert_front(self):
        ll = LinkedList()
        ll.insert_front(1)
        self.assertFalse(ll.is_empty)

    def test_list_insert_after(self):
        ll = LinkedList()
        ll.insert_front(1)
        ll.insert_after(2, 1)
        out = list(ll.traverse())
        self.assertListEqual(out, [1, 2])

    def test_list_delete(self):
        ll = LinkedList()
        ll.insert_front(1)
        ll.insert_front(2)
        ll.insert_after(3, 1)
        ll.insert_after(4, 2)
        self.assertListEqual(list(ll.traverse()), [2, 4, 1, 3])

        ll.delete(ll.search(1))
        self.assertListEqual(list(ll.traverse()), [2, 4, 3])

        ll.delete(ll.search(2))
        self.assertListEqual(list(ll.traverse()), [4, 3])
