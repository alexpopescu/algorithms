import math
import queue

from common import Timer


class Node:
    def __init__(self, data, edges=None):
        self.data = data
        self.edges = edges or []

    def __repr__(self):
        return '{} -> {}'.format(self.data,
                                 ', '.join(['{}({})'.format(*e) if e[1] is not None else str(e[0]) for e in self.edges])
                                 if self.edges else '-')


class Graph:
    def __init__(self, is_directed=False, is_weighted=False):
        self.is_directed = is_directed
        self.is_weighted = is_weighted
        self.nodes = []
        self.edges = []

    def __repr__(self):
        return '\n'.join('{}: {!r}'.format(i, n) for i, n in enumerate(self.nodes))

    def add_nodes(self, *args):
        for arg in args:
            self.nodes.append(Node(arg))

    def add_edges(self, *args):
        for edge in args:
            if self.is_weighted or (len(edge) == 3 and edge[2] is None):
                from_idx, to_idx, weight = edge
            else:
                from_idx, to_idx = edge
                weight = None

            self.edges.append((from_idx, to_idx, weight))
            self.nodes[from_idx].edges.append((to_idx, weight))
            if not self.is_directed:
                self.nodes[to_idx].edges.append((from_idx, weight))

    def find_index(self, data):
        for index in range(len(self.nodes)):
            if self.nodes[index].data == data:
                return index

    def breadth_first_search(self, data):
        n = len(self.nodes)
        visited = [False] * n
        distance = [math.inf] * n
        predecessor = [None] * n

        index = self.find_index(data)
        visited[index] = True
        distance[index] = 0

        q = queue.Queue()
        q.put_nowait(index)
        while not q.empty():
            current = q.get_nowait()
            for connected, weight in self.nodes[current].edges:
                if not visited[connected]:
                    visited[connected] = True
                    distance[connected] = distance[current] + 1
                    predecessor[connected] = current
                    q.put_nowait(connected)

        return distance, predecessor

    def depth_first_search(self, order_index=None):
        n = len(self.nodes)
        visited = [0] * n
        predecessor = [None] * n
        discovered = [0] * n
        finished = [0] * n

        def visit(current, timestamp=0):
            visited[current] = 1
            timestamp += 1
            discovered[current] = timestamp

            for connected, weight in self.nodes[current].edges:
                if visited[connected] == 0:
                    predecessor[connected] = current
                    timestamp = visit(connected, timestamp)

            timestamp += 1
            visited[current] = 2
            finished[current] = timestamp
            return timestamp

        t = 0
        if order_index is None:
            order_index = range(n)
        for i in order_index:
            if visited[i] == 0:
                t = visit(i, t)

        return discovered, finished, predecessor

    def topological_sort(self):
        _, finished, _ = self.depth_first_search()
        indexed = sorted(range(len(finished)), key=lambda k: finished[k], reverse=True)
        return [self.nodes[i] for i in indexed]

    def get_transpose(self):
        gt = Graph(is_directed=self.is_directed)
        gt.add_nodes(*[node.data for node in self.nodes])
        gt.add_edges(*[(j, i, w) for i, j, w in self.edges])
        return gt

    def strongly_connected_components(self):
        _, finished, _ = self.depth_first_search()
        indexed = sorted(range(len(finished)), key=lambda k: finished[k], reverse=True)
        gt = self.get_transpose()
        _, _, predecessor = gt.depth_first_search(order_index=indexed)
        components = []
        remaining = []
        for i, x in enumerate(predecessor):
            if x is None:
                components.append([i])
            else:
                remaining.append((i, x))
        current = 0
        while current > -1:
            child, parent = remaining[current]
            found = False
            for comp in components:
                if parent in comp:
                    comp.append(child)
                    del remaining[current]
                    if current >= len(remaining):
                        current -= 1
                        found = True
                        break
            if not found:
                current += 1
        return [[self.nodes[i].data for i in c] for c in components]

    def minimum_spanning_trees_kruskal(self):
        tree = []
        sets = []
        for index in range(len(self.nodes)):
            sets.append({index})
        edges = sorted(self.edges, key=lambda x: x[2])
        for i, j, w in edges:
            set_i = None
            set_j = None
            for set_i in sets:
                if i in set_i:
                    break
            for set_j in sets:
                if j in set_j:
                    break
            if set_i is not set_j:
                tree.append((i, j))
                set_i |= set_j
                sets.remove(set_j)
        return tree

    def minimum_spanning_trees_prim(self, root):
        root_index = self.find_index(root)
        n = len(self.nodes)
        cost = [math.inf] * n
        predecessor = [None] * n
        cost[root_index] = 0
        tree_edges = []

        q = list(range(0, n))
        while len(q) > 0:
            min_cost = math.inf
            min_index = -1
            for i, c in enumerate(cost):
                if i in q and c < min_cost:
                    min_cost = c
                    min_index = i
            q.remove(min_index)
            if predecessor[min_index] is not None:
                tree_edges.append((predecessor[min_index], min_index))
            for edge in self.nodes[min_index].edges:
                if edge[0] in q and edge[1] < cost[edge[0]]:
                    predecessor[edge[0]] = min_index
                    cost[edge[0]] = edge[1]
        return tree_edges, predecessor

    def shortest_path_bellman_ford(self, root):
        root_index = self.find_index(root)
        n = len(self.nodes)
        distance = [math.inf] * n
        predecessor = [None] * n
        distance[root_index] = 0

        for _ in range(0, n-1):
            for i, j, w in self.edges:
                if distance[j] > distance[i] + w:
                    distance[j] = distance[i] + w
                    predecessor[j] = i
        for i, j, w in self.edges:
            if distance[j] > distance[i] + w:
                return None
        return distance, predecessor

    def shortest_path_dijkstra(self, root):
        root_index = self.find_index(root)
        n = len(self.nodes)
        distance = [math.inf] * n
        predecessor = [None] * n
        distance[root_index] = 0

        computed = []
        q = list(range(0, n))
        while len(q) > 0:
            min_cost = math.inf
            min_index = -1
            for i, c in enumerate(distance):
                if i in q and c < min_cost:
                    min_cost = c
                    min_index = i
            q.remove(min_index)
            computed.append(min_index)
            for j, w in self.nodes[min_index].edges:
                if distance[j] > distance[min_index] + w:
                    distance[j] = distance[min_index] + w
                    predecessor[j] = min_index
        return distance, predecessor


if __name__ == '__main__':  # pragma: no cover
    with Timer('Breadth First Search'):
        g = Graph()
        g.add_nodes('r', 's', 't', 'u', 'v', 'w', 'x', 'y')
        g.add_edges((0, 1), (4, 0), (1, 5), (2, 5), (2, 3), (2, 6), (3, 6), (3, 7), (5, 6), (6, 7))
        dist, pred = g.breadth_first_search('s')
        print(dist, pred)

    with Timer('Depth First Search'):
        g = Graph(is_directed=True)
        g.add_nodes('u', 'v', 'w', 'x', 'y', 'z')
        g.add_edges((0, 1), (0, 3), (1, 4), (2, 4), (2, 5), (3, 1), (4, 3), (5, 5))
        disc, finish, pred = g.depth_first_search()
        print(disc, finish, pred)

    with Timer('Topological Sort'):
        g = Graph(is_directed=True)
        # g.add_nodes('shirt', 'tie', 'jacket', 'belt', 'watch', 'undershorts', 'pants', 'shoes', 'socks')
        # g.add_edges((0, 1), (1, 2), (0, 3), (3, 2), (5, 6), (5, 7), (6, 7), (6, 3), (8, 7))
        g.add_nodes('a', 'b', 'c', 'd', 'e', 'f')
        g.add_edges((0, 3), (5, 1), (1, 3), (5, 0), (3, 2))
        result = g.topological_sort()
        print([r.data for r in result])

    with Timer('Transpose Graph'):
        g = Graph(is_directed=True)
        g.add_nodes('u', 'v', 'w', 'x', 'y', 'z')
        g.add_edges((0, 1), (0, 3), (1, 4), (2, 4), (2, 5), (3, 1), (4, 3), (5, 5))
        print(g.get_transpose())

    with Timer('Strongly Connected Components'):
        g = Graph(is_directed=True)
        g.add_nodes('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h')
        g.add_edges((0, 1), (1, 2), (1, 4), (1, 5), (2, 3), (2, 6), (3, 2),
                    (3, 7), (4, 0), (4, 5), (5, 6), (6, 5), (6, 7), (7, 7))
        print(g.strongly_connected_components())

    with Timer('Minimum Spanning Trees - Kruskal'):
        g = Graph(is_weighted=True)
        g.add_nodes('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i')
        g.add_edges((0, 1, 4), (0, 7, 8), (1, 2, 8), (1, 7, 11), (2, 3, 7), (2, 5, 4), (2, 8, 2),
                    (3, 4, 9), (3, 5, 14), (4, 5, 10), (5, 6, 2), (6, 7, 1), (6, 8, 6), (7, 8, 7))
        print(g.minimum_spanning_trees_kruskal())

    with Timer('Minimum Spanning Trees - Prim'):
        g = Graph(is_weighted=True)
        g.add_nodes('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i')
        g.add_edges((0, 1, 4), (0, 7, 8), (1, 2, 8), (1, 7, 11), (2, 3, 7), (2, 5, 4), (2, 8, 2),
                    (3, 4, 9), (3, 5, 14), (4, 5, 10), (5, 6, 2), (6, 7, 1), (6, 8, 6), (7, 8, 7))
        print(g.minimum_spanning_trees_prim('a'))

    with Timer('Shortest Path - Bellman-Ford'):
        g = Graph(is_directed=True, is_weighted=True)
        g.add_nodes('s', 't', 'x', 'y', 'z')
        g.add_edges((0, 1, 6), (0, 3, 7), (1, 2, 5), (1, 3, 8), (1, 4, -4),
                    (2, 1, -2), (3, 2, -3), (3, 4, 9), (4, 0, 2), (4, 2, 7))
        print(g.shortest_path_bellman_ford('s'))

    with Timer('Shortest Path - Dijkstra'):
        g = Graph(is_directed=True, is_weighted=True)
        g.add_nodes('s', 't', 'x', 'y', 'z')
        g.add_edges((0, 1, 10), (0, 3, 5), (1, 2, 1), (1, 3, 2), (2, 4, 4),
                    (3, 1, 3), (3, 2, 9), (3, 4, 2), (4, 0, 7), (4, 2, 6))
        print(g.shortest_path_dijkstra('s'))
