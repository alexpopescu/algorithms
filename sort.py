import math
import operator

from common import Timer, random_numbers


def bubblesort(array, comparison=operator.lt, in_place=False):
    result = array if in_place else list(array)

    n = len(result)
    for i in range(0, n - 1):
        for j in range(n - 1, i, -1):
            if comparison(result[j], result[j - 1]):
                result[j], result[j - 1] = result[j - 1], result[j]

    return result


def selectionsort(array, comparison=operator.lt, in_place=False):
    result = array if in_place else list(array)

    n = len(result)
    for i in range(0, n - 1):
        min_val = result[i]
        min_idx = i
        for j in range(i+1, n):
            if comparison(result[j], min_val):
                min_val, min_idx = result[j], j
        result[i], result[min_idx] = result[min_idx], result[i]

    return result


def insertionsort(array, comparison=operator.lt, in_place=False):
    result = array if in_place else list(array)

    for j in range(1, len(result)):
        key = result[j]
        i = j - 1
        while i >= 0 and comparison(key, result[i]):
            result[i + 1] = result[i]
            i -= 1
        result[i + 1] = key
    return result


def mergesort(array, start=None, end=None, comparison=operator.lt, in_place=False):
    result = array if in_place else list(array)

    if start is None:
        start = 0
    if end is None:
        end = len(result) - 1

    if start < end:
        split = int((start + end) / 2)
        left = mergesort(result, start, split, comparison=comparison, in_place=in_place)
        right = mergesort(result, split + 1, end, comparison=comparison, in_place=in_place)

        n1 = len(left)
        n2 = len(right)

        i = j = 0
        for k in range(start, end + 1):
            if j == n2 or (i < n1 and comparison(left[i], right[j])):
                result[k] = left[i]
                i += 1
            else:
                result[k] = right[j]
                j += 1

    return result[start:end + 1]


def heapsort(array, comparison=operator.lt, in_place=False):
    def left(index):
        return 2 * index + 1

    def right(index):
        return 2 * index + 2

    def heapify(array, i, length):
        l, r = left(i), right(i)
        largest = l if l < length and comparison(array[i], array[l]) else i
        if r < length and comparison(array[largest], array[r]):
            largest = r
        if largest != i:
            array[i], array[largest] = array[largest], array[i]
            heapify(array, largest, length)

    result = array if in_place else list(array)

    # build max heap
    for i in range(math.floor(len(result) / 2), -1, -1):
        heapify(result, i, len(result))

    for i in range(len(result)-1, 0, -1):
        result[0], result[i] = result[i], result[0]
        heapify(result, 0, i)

    return result


def quicksort(array, start=None, end=None, comparison=operator.lt, in_place=False):
    def partition(array, start, end):
        x = array[end]
        i = start-1
        for j in range(start, end):
            if comparison(array[j], x):
                i += 1
                array[i], array[j] = array[j], array[i]
        array[i+1], array[end] = array[end], array[i+1]
        return i+1

    result = array if in_place else list(array)

    if start is None:
        start = 0
    if end is None:
        end = len(result) - 1

    if start < end:
        q = partition(result, start, end)
        quicksort(result, start, q-1, comparison=comparison, in_place=True)
        quicksort(result, q+1, end, comparison=comparison, in_place=True)

    return result


def countingsort(array):
    n = len(array)
    m = max(array) + 1
    result = [0] * n
    counts = [0] * m

    for i in range(0, n):
        counts[array[i]] += 1
    for i in range(1, m):
        counts[i] += counts[i-1]

    for j in range(n-1, -1, -1):
        result[counts[array[j]]-1] = array[j]
        counts[array[j]] -= 1

    return result


def radixsort(array):
    n = len(array)
    digits = len(str(max(array)))
    result = list(array)

    for d in range(0, digits+1):
        buckets = [[], [], [], [], [], [], [], [], [], []]
        for i in range(0, n):
            buckets[int((result[i] // 10**(d-1)) % 10)].append(result[i])
        result = []
        for bucket in buckets:
            result += bucket

    return result


def minmax(array):
    def order(a, b):
        return (a, b) if a < b else (b, a)

    n = len(array)
    if n == 0:
        return None, None
    if n % 2 == 0:
        min_, max_ = order(array[0], array[1])
        start = 2
    else:
        min_ = max_ = array[0]
        start = 1

    for i in range(start, len(array), 2):
        smaller, larger = order(array[i], array[i+1])
        if smaller < min_:
            min_ = smaller
        if larger > max_:
            max_ = larger

    return min_, max_


if __name__ == '__main__':  # pragma: no cover
    with Timer('Generating Random list'):
        numbers = random_numbers()
    with Timer('Bubble Sort'):
        x1 = bubblesort(numbers)
    with Timer('Selection Sort'):
        x2 = selectionsort(numbers)
    with Timer('Insertion Sort'):
        x3 = insertionsort(numbers)
    with Timer('Merge Sort'):
        x4 = mergesort(numbers)
    with Timer('Heap Sort'):
        x5 = heapsort(numbers)
    with Timer('Quick Sort'):
        x6 = quicksort(numbers)
    with Timer('Counting Sort'):
        x7 = countingsort(numbers)
    with Timer('Radix Sort'):
        x8 = radixsort(numbers)
    with Timer('MinMax'):
        m, M = minmax(numbers)
    # print(m, M)
    # print(x2)
    # print(numbers)
