import re
from collections import defaultdict

from common import Timer


def hanoi(n, a, b, aux):
    if n == 1:
        print('{} -> {}'.format(a, b))
        return
    hanoi(n-1, a, aux, b)
    hanoi(1, a, b, aux)
    hanoi(n-1, aux, b, a)


def permute_rec(s):
    n = len(s)
    if n <= 1:
        return [s]
    p = permute_rec(s[1:])
    res = []
    for ps in p:
        for i in range(0, n):
            res.append(ps[0:i] + s[0] + ps[i:])
    return res


def permute_dup(s):
    def do_permute(freq):
        if not freq:
            return ['']
        res = []
        for c, f in freq.items():
            freq2 = dict(freq)
            if f == 1:
                del freq2[c]
            else:
                freq2[c] = f-1
            perm = do_permute(freq2)
            for w in perm:
                res.append(c + w)
        return res

    freq_table = defaultdict(int)
    for ch in s:
        freq_table[ch] += 1
    return do_permute(freq_table)


def permute_ite(s):
    res = ['']
    for c in s:
        res2 = []
        for ps in res:
            for i in range(len(ps)+1):
                res2.append(ps[0:i] + c + ps[i:])
        res = res2
    return res


def parens(n):
    if n <= 0:
        return ['']
    if n == 1:
        return ['()']
    res = []
    for x in parens(n-1):
        res.append('({})'.format(x))
        res.append('()' + x)
        if x[0:2] != '()':
            res.append(x + '()')
    return res


def change(n, denoms, memo, d_idx=0):
    if n < 0:
        return 0
    if n == 0:
        return 1
    if d_idx >= len(denoms)-1:
        return 1
    d_val = denoms[d_idx]
    res = 0
    for x in range(0, n+1, d_val):
        res += change(n-x, denoms, memo, d_idx+1)
    memo[n][d_idx] = res
    return res


def queens(n, results, cols=None, curr=0):
    def is_valid(x):
        for i, v in enumerate(cols[0:curr]):
            if v == x or i+v == curr+x or v-i == x-curr:
                return False
        return True

    if curr >= n:
        results.append(list(cols))
    if cols is None:
        cols = [0] * n
    for x in range(n):
        if is_valid(x):
            cols[curr] = x
            queens(n, results, cols, curr+1)


def english_int(n):
    nums = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    teens = ['', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
    tens = ['', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']
    muls = ['', 'thousand', 'million', 'billion']

    if n == 0:
        return ''

    if n > 999:
        s = []
        c = 0
        while n > 0:
            x = n % 1000
            if x > 0:
                s.append(english_int(x) + ' ' + muls[c])
            c += 1
            n = n // 1000
        return ', '.join(reversed(s))

    unit = n % 10
    ten = (n // 10) % 10
    hundred = n // 100
    s = []
    if hundred > 0:
        s.append(nums[hundred])
        s.append('hundred')
    if ten == 1:
        s.append(teens[unit]) if unit > 0 else 'ten'
    elif ten > 1:
        s.append(tens[ten])
    if ten != 1 and unit > 0:
        s.append(nums[unit])
    return ' '.join(s)


def calculator(s):
    if '+' in s:
        a = s.split('+')
        return sum(calculator(x) for x in a)

    if '-' in s:
        a = s.split('-')
        res = calculator(a[0])
        for x in a[1:]:
            res -= calculator(x)
        return res

    if '*' in s or '/' in s:
        a = re.split('[*/]', s)
        res = calculator(a[0])
        s = s[len(a[0]):]
        for x in a[1:]:
            if s[0] == '*':
                res *= calculator(x)
            elif s[0] == '/':
                res /= calculator(x)
            s = s[len(x)+1:]
        return res

    return int(s)


if __name__ == '__main__':  # pragma: no cover
    with Timer('Towers of Hanoi'):
        hanoi(3, 'a', 'b', 'aux')
    with Timer('Permutations of string (recursive)'):
        print(permute_rec('abcd'))
    with Timer('Permutations of string (iterative)'):
        print(permute_ite('abcd'))
    with Timer('Permutations of string (excluding duplicates)'):
        print(permute_dup('aaabb'))
    with Timer('Parens'):
        print(parens(4))
    with Timer('Change'):
        n = 25
        denoms = [25, 10, 5, 1]
        memo = [[0] * len(denoms) for _ in range(n+1)]
        print(change(n, denoms, memo))
    with Timer('Queens'):
        res = []
        queens(8, res)
        print(res)
    with Timer('English Int'):
        print(english_int(1724))
        print(english_int(234714))
        print(english_int(2316714))
    with Timer('Calculator'):
        print(calculator('2*3+5/6*3+15'))
