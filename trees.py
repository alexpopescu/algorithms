from common import Timer, random_numbers


class TreeNode:
    def __init__(self, data=None):
        self.data = data
        self.left = None
        self.right = None
        self.parent = None


class ColorTreeNode(TreeNode):
    def __init__(self, data=None, color=None):
        super().__init__(data)
        self.color = color


class BinarySearchTree:
    def __init__(self):
        self.root = None

    def walk(self, node=None, order='in'):
        node = node or self.root
        if node is not None:
            if order == 'pre':
                yield node.data
            if node.left is not None:
                for _ in self.walk(node.left, order=order):
                    yield _
            if order == 'in':
                yield node.data
            if node.right is not None:
                for _ in self.walk(node.right, order=order):
                    yield _
            if order == 'post':
                yield node.data

    def search_recursive(self, data, node=None):
        node = node or self.root
        if node is None or data == node.data:
            return node
        next_node = node.left if data < node.data else node.right
        if next_node is None:
            return None
        return self.search_recursive(data, next_node)

    def search(self, data, node=None):
        node = node or self.root
        while node is not None and data != node.data:
            node = node.left if data < node.data else node.right
        return node

    def min(self, node=None):
        node = node or self.root
        if node is None:
            return None
        while node.left is not None:
            node = node.left
        return node

    def max(self, node=None):
        node = node or self.root
        if node is None:
            return None
        while node.right is not None:
            node = node.right
        return node

    def successor(self, node):
        if node is None:
            return None
        if node.right is not None:
            return self.min(node.right)
        p = node.parent
        while p is not None and node is p.right:
            node = p
            p = p.parent
        return p

    def predecessor(self, node):
        if node is None:
            return None
        if node.left is not None:
            return self.max(node.left)
        p = node.parent
        while p is not None and node is p.left:
            node = p
            p = p.parent
        return p

    def insert(self, node):
        if not isinstance(node, TreeNode):
            node = TreeNode(node)

        parent = None
        current = self.root

        while current is not None:
            parent = current
            current = current.left if node.data < current.data else current.right
        node.parent = parent
        if parent is None:
            self.root = node
        else:
            if node.data < parent.data:
                parent.left = node
            else:
                parent.right = node

    def delete(self, node):
        if node.left is None or node.right is None:
            replacement = node
        else:
            replacement = self.successor(node)
        current = replacement.left if replacement.left is not None else replacement.right
        if current is not None:
            current.parent = replacement.parent

        if replacement.parent is None:
            self.root = current
        else:
            if replacement is replacement.parent.left:
                replacement.parent.left = current
            else:
                replacement.parent.right = current

        if replacement is not node:
            node.data = replacement.data


class RedBlackTree:
    RED = 1
    BLACK = 0

    def __init__(self):
        self.root = None

    def __rotate_left(self, node):
        other = node.right
        node.right = other.left
        if other.left is not None:
            other.left.parent = node
        other.parent = node.parent
        if node.parent is None:
            self.root = other
        else:
            if node == node.parent.left:
                node.parent.left = other
            else:
                node.parent.right = other
        other.left = node
        node.parent = other

    def __rotate_right(self, node):
        other = node.left
        node.left = other.right
        if other.right is not None:
            other.right.parent = node
        other.parent = node.parent
        if node.parent is None:
            self.root = other
        else:
            if node == node.parent.right:
                node.parent.right = other
            else:
                node.parent.left = other
        other.right = node
        node.parent = other

    def __fixup(self, node):
        while node.parent.color == self.RED:
            if node.parent is node.parent.parent.left:
                pass

    def insert(self, node):
        if not isinstance(node, ColorTreeNode):
            node = ColorTreeNode(node, self.RED)

        parent = None
        current = self.root
        while current is not None:
            parent = current
            current = current.left if node.data < current.data else current.right

        node.parent = parent
        if parent is None:
            self.root = node
        else:
            if node.data < parent.data:
                parent.left = node
            else:
                parent.right = node

        self.__fixup(node)


if __name__ == '__main__':  # pragma: no cover
    with Timer('Generating Random list'):
        numbers = random_numbers()

    with Timer('Binary Search Tree test'):
        t = BinarySearchTree()
        for x in numbers:
            t.insert(x)
        num_list = t.walk()
