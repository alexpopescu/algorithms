import math

from common import Timer


def assembly_line_scheduling(assembly, transfer, entry, exit, nodes):
    fastest = [[0] * nodes, [0] * nodes]
    line = [[0] * nodes, [0] * nodes]

    fastest[0][0] = entry[0] + assembly[0][0]
    fastest[1][0] = entry[1] + assembly[1][0]

    for j in range(1, nodes):
        if fastest[0][j - 1] + assembly[0][j] <= fastest[1][j - 1] + transfer[1][j - 1] + assembly[0][j]:
            fastest[0][j] = fastest[0][j - 1] + assembly[0][j]
            line[0][j] = 0
        else:
            fastest[0][j] = fastest[1][j - 1] + transfer[1][j - 1] + assembly[0][j]
            line[0][j] = 1

        if fastest[1][j - 1] + assembly[1][j] <= fastest[0][j - 1] + transfer[0][j - 1] + assembly[1][j]:
            fastest[1][j] = fastest[1][j - 1] + assembly[1][j]
            line[1][j] = 1
        else:
            fastest[1][j] = fastest[0][j - 1] + transfer[0][j - 1] + assembly[1][j]
            line[1][j] = 0

    if fastest[0][nodes - 1] + exit[0] <= fastest[1][nodes - 1] + exit[1]:
        fastest_exit = fastest[0][nodes - 1] + exit[0]
        line_exit = 0
    else:
        fastest_exit = fastest[1][nodes - 1] + exit[1]
        line_exit = 1

    route = []
    i = line_exit
    route.append(i)
    for j in range(nodes - 1, 0, -1):
        i = line[i][j]
        route.append(i)

    return fastest_exit, line_exit, list(reversed(route))


def matrix_chain_order(p):
    n = len(p) - 1
    m = []
    s = []
    for i in range(0, n):
        m.append([0] * n)
        s.append([0] * n)
        m[i][i] = 0
    for chain_length in range(2, n + 1):
        for i in range(0, n - chain_length + 1):
            j = i + chain_length - 1
            m[i][j] = math.inf
            for k in range(i, j):
                q = m[i][k] + m[k + 1][j] + p[i] * p[k + 1] * p[j + 1]
                if q < m[i][j]:
                    m[i][j] = q
                    s[i][j] = k
    return m, s


def get_optimal_matrix_chain(s, i=None, j=None):
    if i is None:
        i = 0
    if j is None:
        j = len(s) - 1

    if i == j:
        return 'A{}'.format(i)
    return '({}{})'.format(
        get_optimal_matrix_chain(s, i, s[i][j]),
        get_optimal_matrix_chain(s, s[i][j] + 1, j)
    )


def longest_common_subsequence(s1, s2):
    n1 = len(s1) + 1
    n2 = len(s2) + 1
    length = []

    for i in range(0, n1):
        length.append([0] * n2)
    for i in range(1, n1):
        for j in range(1, n2):
            if s1[i - 1] == s2[j - 1]:
                length[i][j] = length[i - 1][j - 1] + 1
            elif length[i - 1][j] >= length[i][j - 1]:
                length[i][j] = length[i - 1][j]
            else:
                length[i][j] = length[i][j - 1]
    return length


def get_longest_common_subsequence(length_table, s1, i, j):
    if i == 0 or j == 0:
        return ''
    if length_table[i][j] == length_table[i - 1][j]:
        return get_longest_common_subsequence(length_table, s1, i - 1, j)
    if length_table[i][j] == length_table[i][j - 1]:
        return get_longest_common_subsequence(length_table, s1, i, j - 1)
    return get_longest_common_subsequence(length_table, s1, i - 1, j - 1) + s1[i - 1]


def knapsack(items, total):
    # items = [(value, weight), ...]
    n = len(items)
    value_table = {}

    def calculate(item, limit):
        if item < 0 or limit <= 0:
            return 0

        if (item - 1, limit) not in value_table:
            value_table[item - 1, limit] = calculate(item - 1, limit)

        if items[item][0] > limit:
            value_table[item, limit] = value_table[item - 1, limit]
        else:
            if (item - 1, limit - items[item][0]) not in value_table:
                value_table[item - 1, limit - items[item][0]] = calculate(item - 1, limit - items[item][0])

            value_table[item, limit] = max(value_table[item - 1, limit], value_table[item - 1, limit - items[item][0]] + items[item][1])

        return value_table[item, limit]

    calculate(n-1, total)
    return value_table


def get_knapsack_contents(items, value_table):
    result = []
    item, limit = max(value_table.keys())
    while item >= 0 and limit > 0:
        if value_table[item, limit] != value_table[item - 1, limit]:
            result.append(items[item])
            limit -= items[item][0]
        item -= 1
    return result


if __name__ == '__main__':  # pragma: no cover
    with Timer('Assembly Line Scheduling'):
        fastest = assembly_line_scheduling(
            assembly=[[7, 9, 3, 4, 8, 4], [8, 5, 6, 4, 5, 7]],
            transfer=[[2, 3, 1, 3, 4], [2, 1, 2, 2, 1]],
            entry=[2, 4], exit=[3, 2], nodes=6
        )
        print(fastest)

    with Timer('Optimal Matrix Chain'):
        result = matrix_chain_order([30, 35, 15, 5, 10, 20, 25])
        print(result)
        print(get_optimal_matrix_chain(result[1]))

    with Timer('Longest Common Subsequence'):
        s1 = 'abcbdab'
        s2 = 'bdcaba'
        result = longest_common_subsequence(s1, s2)
        print(result)
        print(get_longest_common_subsequence(result, s1, len(s1), len(s2)))

    with Timer('Knapsack'):
        # items = [(10, 60), (20, 100), (30, 120)]
        # total = 50
        items = [(23, 505), (26, 352), (20, 458), (18, 220), (32, 354), (27, 414), (29, 498), (26, 545), (30, 473), (27, 543)]
        total = 67
        value_table = knapsack(items=items, total=total)
        result = get_knapsack_contents(items, value_table)
        print(value_table)
        print('{} -> {}'.format(result, sum(_[1] for _ in result)))
