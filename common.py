import time
from random import seed, randint


def random_numbers(min_val=0, max_val=10000, length=10000, random_state=None):
    if random_state is not None:
        seed(random_state)
    return [randint(min_val, max_val) for _ in range(length)]


class Timer:
    def __init__(self, name, callback=None):
        self.name = name
        self.callback = callback or self.__callback
        self.start_time = 0

    def __callback(self, duration):
        print('{} took {:.8f} second(s)'.format(self.name, duration))

    def __enter__(self):
        self.start_time = time.time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        duration = time.time() - self.start_time
        self.callback(duration)
